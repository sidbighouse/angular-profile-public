var app = angular.module('profile', ['ngAnimate']);
var tabs = [
    {
        item: 'Home',
        templateUrl: 'components/home/home.html'
    }, {
        item: 'Portfolio',
        templateUrl: 'components/portfolio/portfolio.html'
    }, {
        item: 'About',
        templateUrl: 'components/about/about.html'
    }, {
        item: 'Contact',
        templateUrl: 'components/contact/contact.html'
    }
];

app.controller('MainCtrl', ['$scope', function ($scope) {
    $scope.tabs = tabs;

    $scope.selectTab = function (setTab) {
        $scope.tab = setTab;
    };
    $scope.isSelected = function (checkTab) {
        return $scope.tab === checkTab;
    };
}]);

/**
 * Social media controller
 */
//angular.module('profile')

/**
 * Social media directives
 */
angular.module('profile')
    .directive('socialMedia', function () {
        return {
            restrict: 'E',
            templateUrl: 'shared/socialmedia/social-media.html',
            link: function (scope) {
                scope.socialMediaAccs = [
                    { id: "twitter", name: "Twitter", account: "https://twitter.com/sidcasagrande" },
                    { id: "linkedin", name: "LinkedIn", account: "https://au.linkedin.com/in/sidneycasagrande" },
                    { id: "bitbucket", name: "Bitbucket", account: "https://bitbucket.org/sidbighouse" }
                ];
            }
        }
    });

/**
 * About controllers
 */
angular.module('profile')
	.controller('AboutCtrl', ['$scope', function ($scope) {
		$scope.aboutMe = {
			title: 'About Franz Kafka',
			text: 'One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. ' +
                'He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, ' +
                'slightly domed and divided by arches into stiff sections. The bedding was hardly able to cover it and seemed ready to slide off any moment. ' +
                'His many legs, pitifully thin compared with the size of the rest of him, waved about helplessly as he looked. ' +
                '\"What\'s happened to me?" he thought. It wasn\'t a dream. His room, a proper human room although a little too small, ' +
                'lay peacefully between its four familiar walls. A collection of textile samples lay spread out on the table - ' +
                'Samsa was a travelling salesman - and above it there hung a picture that he had recently cut out of an illustrated magazine and housed in a nice, ' +
                'gilded frame. It showed a lady fitted out with a fur hat and fur boa who sat upright, ' +
                'raising a heavy fur muff that covered the whole of her lower arm towards the viewer. Gregor then turned to look out the window at the dull weather. '
		}
	}]);

/**
 * About directives
 */
//angular.module('profile')

/**
 * Contact form controllers
 */
angular.module('profile')
    .controller('ContactFormCtrl', ['$scope', '$window', function ($scope, $window) {
        $scope.content = {
            title: "Contact",
            text: "Any questions about myself or my professional experience? Get in touch by filling out this form.",
            supplementaryText: "This form will open up your default mail client."
        };
        $scope.contact = {name: "", email: "", text: ""};
        $scope.sendMail = function () {
            var contactFormDetails = {
                myEmail: "example@test123.com",
                subject: "Kafka's profile: " + $scope.contact.name + " has a request.",
                body: "Hi Franz, my name is " +
                    $scope.contact.name + ".%0D%0A%0D%0AI'm just visiting your website and I would like to talk to you about:%0D%0A%0D%0A'" +
                    $scope.contact.text + "'%0D%0A%0D%0APlease contact me on:%0D%0A%0D%0A" +
                    $scope.contact.email
            };

            $window.location = "mailto:" + contactFormDetails.myEmail + "?subject=" + contactFormDetails.subject + "&body=" + contactFormDetails.body;
        };
    }]);

/**
 * Contact form directives
 */
angular.module('profile')
    .directive('contactForm', function () {
        return {
            restrict: 'E',
            controller: 'ContactFormCtrl',
            templateUrl: 'components/contact/contact-form.html'
        }
    });

/**
 * Portfolio controller
 */
angular.module('profile')
    .controller('PortfolioCtrl', ['$scope', function ($scope) {
        $scope.portfolioList = [ {
                id: "One",
                title: "Lorem Ipsum",
                content: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.",
                imgSrc: "resources/imgs/loremipsum.jpg",
                imgAlt: "Lorem Ipsum",
                imgLink: "http://www.lipsum.com/",
                skills: {
                    literature: {
                        title: 'Literature',
                        list: ['Lorem Ipsum Wiki', 'Lipsum']
                    },
                    languages: {
                        title: 'Languages',
                        list: ['Loranian', 'Ipsumish']
                    },
                    origin: {
                        title: 'Origin',
                        list: ['Lorania', 'Ipsum']
                    },
                    religion: {
                        title: 'Religion',
                        list: ['Loripsic', 'Ipslorian']
                    }
                }
            }, {
                id: "Two",
                title: "Franz Kafka",
                content: "One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections. The bedding was hardly able to cover it and seemed ready to slide off any moment. His many legs, pitifully thin compared with the size of the rest of him, waved about helplessly as he looked. \"What\'s happened to me?\" he thought. It wasn\'t a dream.",
                imgSrc: "resources/imgs/kafka.jpg",
                imgAlt: "Franz Kafka",
                imgLink: "http://en.wikipedia.org/wiki/Franz_Kafka",
                skills: {
                    literature: {
                        title: 'Literature',
                        list: ['Der Prozess', 'Der Heizer', 'Das Schloss']
                    },
                    languages: {
                        title: 'Languages',
                        list: ['Czech', 'German']
                    },
                    origin: {
                        title: 'Origin',
                        list: ['Kingdom of Bohemia - Prague', 'Austro-Hungarian Empire']
                    },
                    religion: {
                        title: 'Religion',
                        list: ['Jewish']
                    }
                }
            }, {
                id: "Three",
                title: "Cicero - Not lorem ipsum",
                content: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.",
                imgSrc: "resources/imgs/cicero.jpg",
                imgAlt: "Cicero",
                imgLink: "http://en.wikipedia.org/wiki/Cicero",
                skills: {
                    literature: {
                        title: 'Literature',
                        list: ['Epistulae ad Atticum', 'Laelius de Amicitia', 'Philippicae']
                    },
                    languages: {
                        title: 'Languages',
                        list: ['Latin', 'Old Greek']
                    },
                    origin: {
                        title: 'Origin',
                        list: ['Roman empire - Arpinium', 'Greece - Asia Minor & Rhodes & Athens']
                    },
                    religion: {
                        title: 'Religion',
                        list: ['Roman']
                    }
                }
            }
        ];
    }]);

/**
 * Portfolio directives
 */
//angular.module('profile')

/**
 * Created by Sidney on 3/03/2015.
 * This file is used for any custom jQuery or JavaScript (non-angular)
 */
