/**
 * Portfolio controller
 */
angular.module('profile')
    .controller('PortfolioCtrl', ['$scope', function ($scope) {
        $scope.portfolioList = [ {
                id: "One",
                title: "Lorem Ipsum",
                content: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.",
                imgSrc: "resources/imgs/loremipsum.jpg",
                imgAlt: "Lorem Ipsum",
                imgLink: "http://www.lipsum.com/",
                skills: {
                    literature: {
                        title: 'Literature',
                        list: ['Lorem Ipsum Wiki', 'Lipsum']
                    },
                    languages: {
                        title: 'Languages',
                        list: ['Loranian', 'Ipsumish']
                    },
                    origin: {
                        title: 'Origin',
                        list: ['Lorania', 'Ipsum']
                    },
                    religion: {
                        title: 'Religion',
                        list: ['Loripsic', 'Ipslorian']
                    }
                }
            }, {
                id: "Two",
                title: "Franz Kafka",
                content: "One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections. The bedding was hardly able to cover it and seemed ready to slide off any moment. His many legs, pitifully thin compared with the size of the rest of him, waved about helplessly as he looked. \"What\'s happened to me?\" he thought. It wasn\'t a dream.",
                imgSrc: "resources/imgs/kafka.jpg",
                imgAlt: "Franz Kafka",
                imgLink: "http://en.wikipedia.org/wiki/Franz_Kafka",
                skills: {
                    literature: {
                        title: 'Literature',
                        list: ['Der Prozess', 'Der Heizer', 'Das Schloss']
                    },
                    languages: {
                        title: 'Languages',
                        list: ['Czech', 'German']
                    },
                    origin: {
                        title: 'Origin',
                        list: ['Kingdom of Bohemia - Prague', 'Austro-Hungarian Empire']
                    },
                    religion: {
                        title: 'Religion',
                        list: ['Jewish']
                    }
                }
            }, {
                id: "Three",
                title: "Cicero - Not lorem ipsum",
                content: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.",
                imgSrc: "resources/imgs/cicero.jpg",
                imgAlt: "Cicero",
                imgLink: "http://en.wikipedia.org/wiki/Cicero",
                skills: {
                    literature: {
                        title: 'Literature',
                        list: ['Epistulae ad Atticum', 'Laelius de Amicitia', 'Philippicae']
                    },
                    languages: {
                        title: 'Languages',
                        list: ['Latin', 'Old Greek']
                    },
                    origin: {
                        title: 'Origin',
                        list: ['Roman empire - Arpinium', 'Greece - Asia Minor & Rhodes & Athens']
                    },
                    religion: {
                        title: 'Religion',
                        list: ['Roman']
                    }
                }
            }
        ];
    }]);