/**
 * Contact form directives
 */
angular.module('profile')
    .directive('contactForm', function () {
        return {
            restrict: 'E',
            controller: 'ContactFormCtrl',
            templateUrl: 'components/contact/contact-form.html'
        }
    });