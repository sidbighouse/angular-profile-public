/**
 * Contact form controllers
 */
angular.module('profile')
    .controller('ContactFormCtrl', ['$scope', '$window', function ($scope, $window) {
        $scope.content = {
            title: "Contact",
            text: "Any questions about myself or my professional experience? Get in touch by filling out this form.",
            supplementaryText: "This form will open up your default mail client."
        };
        $scope.contact = {name: "", email: "", text: ""};
        $scope.sendMail = function () {
            var contactFormDetails = {
                myEmail: "example@test123.com",
                subject: "Kafka's profile: " + $scope.contact.name + " has a request.",
                body: "Hi Franz, my name is " +
                    $scope.contact.name + ".%0D%0A%0D%0AI'm just visiting your website and I would like to talk to you about:%0D%0A%0D%0A'" +
                    $scope.contact.text + "'%0D%0A%0D%0APlease contact me on:%0D%0A%0D%0A" +
                    $scope.contact.email
            };

            $window.location = "mailto:" + contactFormDetails.myEmail + "?subject=" + contactFormDetails.subject + "&body=" + contactFormDetails.body;
        };
    }]);