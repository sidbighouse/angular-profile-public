/**
 * Social media directives
 */
angular.module('profile')
    .directive('socialMedia', function () {
        return {
            restrict: 'E',
            templateUrl: 'shared/socialmedia/social-media.html',
            link: function (scope) {
                scope.socialMediaAccs = [
                    { id: "twitter", name: "Twitter", account: "https://twitter.com/sidcasagrande" },
                    { id: "linkedin", name: "LinkedIn", account: "https://au.linkedin.com/in/sidneycasagrande" },
                    { id: "bitbucket", name: "Bitbucket", account: "https://bitbucket.org/sidbighouse" }
                ];
            }
        }
    });