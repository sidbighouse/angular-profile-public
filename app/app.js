var app = angular.module('profile', ['ngAnimate']);
var tabs = [
    {
        item: 'Home',
        templateUrl: 'components/home/home.html'
    }, {
        item: 'Portfolio',
        templateUrl: 'components/portfolio/portfolio.html'
    }, {
        item: 'About',
        templateUrl: 'components/about/about.html'
    }, {
        item: 'Contact',
        templateUrl: 'components/contact/contact.html'
    }
];

app.controller('MainCtrl', ['$scope', function ($scope) {
    $scope.tabs = tabs;

    $scope.selectTab = function (setTab) {
        $scope.tab = setTab;
    };
    $scope.isSelected = function (checkTab) {
        return $scope.tab === checkTab;
    };
}]);