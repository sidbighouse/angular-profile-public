module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        clean: {
            deps: [
                'app/resources/js/dist/**/*.js.map',
                'app/resources/js/dist/**/*.js',
                'app/resources/styles/dist/<%= pkg.name %>.css',
                'app/resources/styles/dist/styles.css.map'
            ],
            sass: [
                'app/resources/styles/dist/styles.css'
            ],
            fonts: [
                'app/resources/styles/fonts/**/*'
            ]
        },
        copy: {
            fontawesome: {
                cwd: 'bower_components/font-awesome/fonts',
                src: ['**/*'],
                dest: 'app/resources/styles/fonts/',
                expand: true
            },
            bootstrapfonts: {
                cwd: 'bower_components/bootstrap/fonts',
                src: ['**/*'],
                dest: 'app/resources/styles/fonts/',
                expand: true
            }
        },
        compress: {
            profile: {
                options: {
                    archive: '../profile.zip'
                },
                files: [{src: ['app/**']}]
            }
        },
        concat: {
            options: {
                separator: "\n\n"
            },
            dist: {
                src: [
                    'app/app.js',
                    'app/shared/**/*.js',
                    'app/components/**/*.js',
                    'app/resources/js/custom/**/*.js'
                ],
                dest: 'app/resources/js/dist/<%= pkg.name %>.js'
            },
            deps: {
                src: [
                    'bower_components/modernizr/modernizr.js',
                    'bower_components/jquery/dist/jquery.js',
                    'bower_components/bootstrap/dist/js/bootstrap.js',
                    'bower_components/angularjs/angular.min.js',
                    'bower_components/angular-animate/angular-animate.min.js'
                ],
                dest: 'app/resources/js/dist/<%= pkg.name %>-deps.js'
            },
            css: {
                src: ['bower_components/bootstrap/dist/css/bootstrap.min.css',
                    'bower_components/font-awesome/css/font-awesome.min.css',
                    'bower_components/bootstrap-social/bootstrap-social.css',
                    'app/resources/styles/dist/styles.css'
                ],
                dest: 'app/resources/styles/dist/<%= pkg.name %>.css'
            },
            moveNg: {
                src: ['bower_components/angularjs/angular.min.js.map'],
                dest: 'app/resources/js/dist/angular.min.js.map'
            },
            moveNgAnim: {
                src: ['bower_components/angular-animate/angular-animate.min.js.map'],
                dest: 'app/resources/js/dist/angular-animate.min.js.map'
            }
        },
        sass: {
            dev: {
                files: {
                    'app/resources/styles/dist/styles.css': 'app/resources/styles/styles.scss'
                }
            }
        },
        watch: {
            scripts: {
                files: ['app/resources/js/**/*.js'],
                tasks: ['concat:dist']
            },
            styles: {
                files: ['app/resources/styles/*.scss'],
                tasks: ['sass']
            }
        }
    });

    //npm tasks
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-ngdocs');

    //tasks
    grunt.registerTask('default', 'Default Task Alias', ['build']);

    grunt.registerTask('build', 'Build the application', [
        'clean',
        'sass:dev',
        'concat',
        'copy',
        'clean:sass'
    ]);

    grunt.registerTask('package', 'Build and package the application',[
        'build',
        'compress'
    ]);
};