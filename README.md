# Single-page app using AngularJS #


## Prerequisites ##
* Install NodeJs and add it to the System Path
* Install Ruby and add it to the System Path


## Initial Project set-up ##
* cd *into workspace location*
* git clone https://bitbucket.org/sidbighouse/angular-profile-public.git
* cd angular-profile-public
* run *npm install*
* run *bower install*


## SASS ##
Sass can be installed using a Ruby gem.

* Run *gem install sass*

* If you get an *unable to download* - error add the unprotected source to Ruby:
* *gem sources -a http://rubygems.org/*
* On linux you might be required to install gems as *sudo* user to access var/lib
* Run *gem install sass* again


## Grunt ##
To run Grunt tasks you will have to install Ruby and Sass

* Run *grunt build*

List of major grunt tasks:

* grunt build
* grunt clean
* grunt package


## HTTP-Server ##
To test all angular functionality properly it is advised to run a local server.
We have already set-up has a http-server utility for that using npm:

* To run: *http-server*
* To access: *http://localhost:8080/app*
* To install if any issues: *npm install -g http-server* (sudo on linux)